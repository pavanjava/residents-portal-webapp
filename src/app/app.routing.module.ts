import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from "./app.component";
import { ContactComponent } from './contact/contact.component';
import {HomeComponent} from "./home/home.component";
import {ConstellationComponent} from "./constellation/constellation.component";
import { FilterComponent } from './filter/filter.component';

const routes : Routes = [
  {path : '', component : HomeComponent},
  {path : 'home', component : HomeComponent},
  {path : 'constellation', component : ConstellationComponent},
  {path : 'filter', component : FilterComponent },
  {path : 'contactUs', component : ContactComponent},
  {path : '**', component : AppComponent}
];

@NgModule({
  imports: [
    NgbModule.forRoot(),
    RouterModule.forRoot(
      routes,
      {enableTracing: true}
    )
  ],
  exports: [
    RouterModule
  ],
  declarations: [

  ]
})
export class AppRoutingModule { }
