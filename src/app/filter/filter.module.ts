import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './filter.component';
import { RouterModule, Routes } from '@angular/router';
import {BuildingsService} from "../common-services/buildings.service";
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgbModule.forRoot()
  ],
  declarations: [FilterComponent],
  providers: [
    BuildingsService
  ]
})
export class FilterModule { }
