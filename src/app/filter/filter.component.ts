import { Component, OnInit } from '@angular/core';
import {BuildingsService} from "../common-services/buildings.service";
import {Response} from "@angular/http";
import {DataRetrievalService} from "../common-services/data.retrieval.service";
import * as data from '../util/mocks-tenants.json';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  private buildingsList : any[];
  private buildingsAndUnits : any[];
  private selectedBuilding : string = "Choose a building ...";
  private selectedUnit : string = "Choose a unit ...";
  private unitsModel : any[];
  private disableGoButton : boolean = true;
  private tenantsList : any[];
  private tenantsMock : {};

  constructor(private dataRetrievalService : DataRetrievalService,
              private buildingsService : BuildingsService) {
  }

  ngOnInit() {
    console.log("*** Inside filter comp ngoninit *** ");
    this.tenantsMock = data;
    this.getBuildingsAndUnits();
  }

  unitsMenuSelectionEvent(newValue) {
    console.log("*** units dropdown event fired ***");
    if(newValue !== "Choose a unit ...") this.selectedUnit = newValue.doorNumber;
    else this.selectedUnit ="Choose a unit ...";
  }

  submitEvent() {
    console.log("*** Submit event fired ***");
    this.getTenantForABuildingUnit();
  }

  menuSelectionEvent(newValue: string) {
    console.log("*** building dropdown event fired ***");
    if(newValue !== "Choose a building ...") this.disableGoButton = false;
    this.selectedBuilding = newValue;
    for(let buildingUnit of this.buildingsAndUnits) {
      if(buildingUnit.buildingName.toString() === this.selectedBuilding) {
        if(!(buildingUnit.units instanceof Array)) { //Should not go into if assuming a building has atleast 2 units
          let arr = [];
          arr.push(buildingUnit.units);
          this.unitsModel = arr;
        } else {
          this.unitsModel = buildingUnit.units;
        }
      }
    }
  }

  getTenantForABuildingUnit() {
    this.dataRetrievalService.getTenantForABuildingUnit(this.selectedBuilding, this.selectedUnit)
      .subscribe((data : Response) => {
        this.setTenantsList(data);
      });
  }

  setTenantsList(data) {
    if(!(data.tenantsList instanceof Array)) {
      let arr = [];
      arr.push(data.tenantsList);
      this.setTenants(arr);
    } else {
      this.setTenants(data.tenantsList);
    }
  }

  setTenants(tenantsArr) {
    this.tenantsList = tenantsArr;
  }

  getBuildingsAndUnits() {
    this.dataRetrievalService.getBuildingsAndUnits()
      .subscribe((data : Response) => {
        this.setBuildingsAndUnits(data);
        this.setAllBuildings(data);
      });
  }

  setBuildingsAndUnits(data) {
    this.buildingsAndUnits = data.buildingsAndUnits;
  }

  setAllBuildings(data) {
    let buildings = [];
    let currentBuildings = this.buildingsService.getBuildings();
    if(currentBuildings === null) {
      for(let buildingUnit of data.buildingsAndUnits) {
        buildings.push(buildingUnit.buildingName.toString());
      }
      this.buildingsService.setBuildings(buildings);
      this.buildingsList = buildings;
    } else {
      this.buildingsList = buildings;
    }
  }
}
