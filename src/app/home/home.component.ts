import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  skyScrapperImagePath : String;

  constructor() {
    this.skyScrapperImagePath = '/assets/images/sky_scapper.png';
  }

  ngOnInit() {
  }

}
