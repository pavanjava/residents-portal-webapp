import { Injectable } from '@angular/core';

@Injectable()
export class BuildingsService {
  private buildings : string[] = null;

  constructor() { }

  getBuildings() {
    return this.buildings;
  }

  setBuildings(buildings : string[]) {
    this.buildings = buildings;
  }
}
