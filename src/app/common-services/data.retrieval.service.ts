import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Rx";
import { environment } from '../../environments/environment';
import { Http, Headers, Response } from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DataRetrievalService {

  envSpecificBaseUrl : string;

  constructor(private http : Http) {
    this.envSpecificBaseUrl = environment.baseUrl;
  }

  getBuildingsAndUnits() {
    return this.http.get(this.envSpecificBaseUrl + '/tenants/buildings-units')
      .map(this.processData)
      .catch(this.handleErrors);
  }

  getTenantForABuildingUnit(building : string, unit : string) {
    if(unit === 'Choose a unit ...') unit = null;
    console.log(unit);
    return this.http.get(this.envSpecificBaseUrl + '/tenants/building/' + building + '/unit/' + unit)
      .map(this.processData)
      .catch(this.handleErrors);
  }

  processData(res : Response) {
    console.log("Successfully recieved the response");
    return res.json();
  }

  handleErrors(error: Response | any) {
    console.log("------- EXCEPTION -------");
    return Observable.throw(error.message);
  }
}
