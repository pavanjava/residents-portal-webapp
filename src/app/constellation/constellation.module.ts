import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ConstellationComponent} from "./constellation.component";
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import {ConstellationService} from "./constellation.service";
import {BuildingsService} from "../common-services/buildings.service";

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    RouterModule
  ],
  declarations: [
    ConstellationComponent
  ],
  providers: [
    ConstellationService,
    BuildingsService
  ]
})
export class ConstellationModule { }
