import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Http, Headers, Response } from '@angular/http'
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ConstellationService {

  envSpecificBaseUrl : string;

  constructor(private http : Http) {
    this.envSpecificBaseUrl = environment.baseUrl;
  }

  ngOnInit() {

  }

  getTestWebservice() {
    return this.http.get(this.envSpecificBaseUrl + '/tenants/test')
      .map(this.processData)
      .catch(this.handleErrors);
  }

  getBuildingsAndUnits() {
    return this.http.get(this.envSpecificBaseUrl + '/tenants/buildings-units')
      .map(this.processData)
      .catch(this.handleErrors);
  }

  processData(res : Response) {
    console.log("Successfully recieved the response");
    return res.json();
  }

  handleErrors(error: Response | any) {
    console.log("------- EXCEPTION -------");
    return Observable.throw(error.message);
  }
}
