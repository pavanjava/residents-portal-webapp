import { Component, OnInit } from '@angular/core';
import {ConstellationService} from "./constellation.service";
import {Response} from "@angular/http";
import { Observable } from 'rxjs';

import {Tenants} from "./Tenants";
import {BuildingsService} from "../common-services/buildings.service";
import * as c3 from 'c3';

@Component({
  selector: 'app-constellation',
  templateUrl: './constellation.component.html',
  styleUrls: ['./constellation.component.css']
})
export class ConstellationComponent implements OnInit {

  private tenants : Tenants = null;
  private buildingsList : any[];
  private buildingsAndUnits : any[];

  constructor(private constellationService : ConstellationService,
              private buildingsService : BuildingsService) {
  }

  ngOnInit() {
    console.log("Inside constellation comp === ");
    this.getTestViewModel();
    this.getBuildingsAndUnits();
  }

  ngAfterViewInit() {
    let chart = c3.generate({
      bindto: '#chart',
      data: {
        columns: [
          ['data1', 30, 200, 100, 400, 150, 250],
          ['data2', 50, 20, 10, 40, 15, 25]
        ]
      }
    });
  }

  getTestViewModel() {
    this.tenants = new Tenants();
    this.constellationService.getTestWebservice()
        .subscribe((data : Tenants) => {
          this.tenants.tenantPhoneNumber = data.tenantPhoneNumber;
          this.tenants.buildingName = data.buildingName;
        });
  }

  getBuildingsAndUnits() {
    this.constellationService.getBuildingsAndUnits()
        .subscribe((data : Response) => {
          this.setBuildingsAndUnits(data);
          this.setAllBuildings(data);
        });
  }

  setBuildingsAndUnits(data) {
    this.buildingsAndUnits = data.buildingsAndUnits;
  }

  setAllBuildings(data) {
    let buildings = [];
    let currentBuildings = this.buildingsService.getBuildings();
    if(currentBuildings === null) {
      for(let buildingUnit of data.buildingsAndUnits) {
        buildings.push(buildingUnit.buildingName.toString());
      }
      this.buildingsService.setBuildings(buildings);
      this.buildingsList = buildings;
    } else {
      this.buildingsList = buildings;
    }
  }

}
